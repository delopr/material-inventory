import React from 'react';
import ReactDOM from 'react-dom';
import 'mobx-react-lite/batchingForReactDom';

import { App } from './app';

ReactDOM.render(<App />, document.getElementById('material-inventory'));
