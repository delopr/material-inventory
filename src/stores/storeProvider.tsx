import React from 'react';
import { MaterialsStore } from './materialsStore';

export const StoreContext = React.createContext<MaterialsStore | null>(null);

export const StoreProvider: React.FC = ({ children }) => (
    <StoreContext.Provider value={new MaterialsStore()}>{children}</StoreContext.Provider>
);
