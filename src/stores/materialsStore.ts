import { observable, action } from 'mobx';
import { parseMaterialRawData } from '../utils/materialsParser';
import { Warehouse } from '../models/warehouse';

export class MaterialsStore {
    @observable
    public warehouses: Warehouse[] = [];

    @action
    public loadRawData = (data: string): void => {
        this.warehouses = parseMaterialRawData(data);
    };
}
