import React from 'react';
import { StoreContext } from './storeProvider';
import { MaterialsStore } from './materialsStore';

export function useMaterialsStore(): MaterialsStore {
    const materialsStore = React.useContext(StoreContext);

    if (!materialsStore) {
        throw new Error('Missing MaterialsStore provider');
    }

    return materialsStore;
}
