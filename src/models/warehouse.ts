import { Material } from './material';

export class Warehouse {
    public materials: Material[];

    get totalCount(): number {
        return this.materials.reduce<number>((prevSum, currMaterial) => prevSum + currMaterial.count, 0);
    }

    constructor(public name: string, materials: Material[]) {
        // merge counts of possible duplicated materials
        this.materials = materials.reduce<Material[]>((prev: Material[], current: Material) => {
            const existingMaterials = prev.filter((material) => material.materialId == current.materialId);

            if (existingMaterials[0]) {
                existingMaterials[0].count += current.count;
                return prev;
            }

            return [...prev, current];
        }, []);
    }
}
