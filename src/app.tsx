import React from 'react';
import { default as bemCssModules } from 'bem-css-modules';
import './app.css';

import { MaterialInventory } from './containers/materialInventory/materialInventory';
import { ErrorBoundary } from './containers/errorBoundary/errorBoundary';
import { StoreProvider } from './stores/storeProvider';

bemCssModules.setSettings({
    modifierDelimiter: '--',
    throwOnError: true,
});

export const App: React.FC = () => (
    <ErrorBoundary>
        <StoreProvider>
            <MaterialInventory />
        </StoreProvider>
    </ErrorBoundary>
);
