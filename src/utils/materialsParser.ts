import { Warehouse } from '../models/warehouse';
import { Material } from '../models/material';

interface MaterialLine {
    materialId: string;
    warehousesCounts: Map<string, number>;
}

/*
    Parses text data from input file into the Warehouse models
*/
export function parseMaterialRawData(rawData: string): Warehouse[] {
    const allMaterialLines = rawData
        .split(/\r?\n/)
        .map((line) => parseLine(line))
        .filter((materialLine) => materialLine !== null) as MaterialLine[];

    const allWarehouseIds = allMaterialLines.map((materialLine) => [...materialLine.warehousesCounts.keys()]).flat();
    const uniqueWarehouseIds = Array.from(new Set(allWarehouseIds));

    return uniqueWarehouseIds.map((warehouseId) => {
        const warehouseMaterials = allMaterialLines
            .filter((materialLine) => materialLine.warehousesCounts.has(warehouseId))
            .map(
                (materialLine) =>
                    new Material(materialLine.materialId, materialLine.warehousesCounts.get(warehouseId)!),
            );

        return new Warehouse(warehouseId, warehouseMaterials);
    });
}

/*
    Parses single line in format:
    Nazwa materiału;ID Materiału;Magazyn,Ilość|Magazyn,Ilość|Magazyn,Ilość
*/
function parseLine(line: string): MaterialLine | null {
    if (!line.length || line.startsWith('#')) {
        return null;
    }

    const [, materialId, warehouses] = line.split(';');
    const entries = warehouses.split('|').map<[string, number]>((warehouseCount) => {
        const [warehouseId, countStr] = warehouseCount.split(',');
        return [warehouseId, parseInt(countStr)];
    });

    return {
        materialId,
        warehousesCounts: new Map(entries),
    };
}
