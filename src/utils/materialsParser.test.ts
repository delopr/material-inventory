import { parseMaterialRawData } from './materialsParser';

test('ignores comment lines', () => {
    const inputLines = `# ignoring this line
#Material A;A;W1,3
#Material B;B;W1,5`;
    const result = parseMaterialRawData(inputLines);

    expect(result).toHaveLength(0);
});

test('reads material id', () => {
    const inputLine = 'Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2';
    const result = parseMaterialRawData(inputLine);

    expect(result).toHaveLength(3);
    expect(result[0].materials).toHaveLength(1);
    expect(result[0].materials[0].materialId).toEqual('COM-123906c');
});

test('reads warehouse name (single)', () => {
    expect(parseMaterialRawData('Generic Wire Pull;COM-123906c;WH-A,10')[0].name).toEqual('WH-A');
});

test('reads warehouse name (tripple)', () => {
    const inputLine = 'Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2';
    const warehouses = parseMaterialRawData(inputLine).map((warehouse) => warehouse.name);

    expect(warehouses).toHaveLength(3);
    expect(warehouses).toContainEqual('WH-A');
    expect(warehouses).toContainEqual('WH-B');
    expect(warehouses).toContainEqual('WH-C');
});

test('calculates warehouse total count', () => {
    const inputLines = `Material A;A;W1,3
Material B;B;W1,5`;
    const result = parseMaterialRawData(inputLines);

    expect(result).toHaveLength(1);
    expect(result[0].totalCount).toEqual(8);
});
