import React from 'react';
import { observer } from 'mobx-react-lite';

import { default as bemCssModules } from 'bem-css-modules';
import { default as WarehousesSummaryModuleCss } from './warehousesSummary.module.css';
import { useMaterialsStore } from '../../stores/hooks';
import { Warehouse } from '../../models/warehouse';
import { WarehouseStats } from '../../components/warehouseStats/warehouseStats';

const style = bemCssModules(WarehousesSummaryModuleCss);

const WarehousesSummary: React.FC = () => {
    const { warehouses } = useMaterialsStore();

    const compareTotalDescNameDesc = (wh1: Warehouse, wh2: Warehouse): number =>
        wh2.totalCount - wh1.totalCount || wh2.name.localeCompare(wh1.name);

    const warehousesStats = warehouses
        .slice()
        .sort(compareTotalDescNameDesc)
        .map((warehouse) => <WarehouseStats key={warehouse.name} warehouse={warehouse} />);

    return (
        <div className={style()}>
            {warehouses.length > 0 ? <h2>Results</h2> : null}
            {warehousesStats}
        </div>
    );
};

const warehousesSummaryConsumer = observer(WarehousesSummary);
export { warehousesSummaryConsumer as WarehousesSummary };
