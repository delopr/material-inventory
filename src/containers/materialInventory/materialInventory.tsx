import React from 'react';
import { default as bemCssModules } from 'bem-css-modules';
import { default as MaterialInventoryModuleCss } from './MaterialInventory.module.css';

import { InputArea } from '../../components/inputArea/inputArea';
import { WarehousesSummary } from '../warehousesSummary/warehousesSummary';

const style = bemCssModules(MaterialInventoryModuleCss);

export const MaterialInventory: React.FC = () => (
    <div className={style()}>
        <h1>Material Inventory</h1>
        <InputArea />
        <WarehousesSummary />
    </div>
);
