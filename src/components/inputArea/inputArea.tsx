import React, { useState } from 'react';
import { FileDrop } from 'react-file-drop';

import { useMaterialsStore } from '../../stores/hooks';
import { default as bemCssModules } from 'bem-css-modules';
import { default as InputAreaModuleCss } from './inputArea.module.scss';
import './fileDrop.scss';

const style = bemCssModules(InputAreaModuleCss);

export const InputArea: React.FC = () => {
    const [prompt, setPrompt] = useState('Please drag and drop here input file');
    const [loadedText, setLoadedText] = useState('');
    const { loadRawData } = useMaterialsStore();

    const onFileDrop = (files: FileList | null) => {
        if (!files) {
            setPrompt('No files were successfully dropped, please try again');
            return;
        }

        const reader = new FileReader();
        reader.onerror = (event) => setPrompt(event.target?.error?.message ?? 'Error occured while reading file');
        reader.onload = (event) => {
            const result = event.target?.result;
            if (typeof result === 'string') {
                try {
                    loadRawData(result);
                    setLoadedText(result);
                    setPrompt('File loaded!');
                } catch (error) {
                    setPrompt(`Incorrect data format ${error}`);
                }
            }
        };

        reader.readAsText(files[0]);
    };

    const onInputFileChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        onFileDrop(event.target.files);
    };

    return (
        <React.Fragment>
            <h2>Input data</h2>
            <FileDrop onDrop={onFileDrop} className={style()}>
                <article className={style('prompt')}>{prompt}</article>
                <input className={style('file')} type="file" onChange={onInputFileChange}></input>
                <pre className={style('loaded-text')}>{loadedText}</pre>
            </FileDrop>
        </React.Fragment>
    );
};
