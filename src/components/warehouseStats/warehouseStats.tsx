import React from 'react';

import { default as bemCssModules } from 'bem-css-modules';
import { default as WarehouseStatsModuleCss } from './warehouseStats.module.scss';
import { Warehouse } from '../../models/warehouse';

export interface WarehouseStatsProps {
    warehouse: Warehouse;
}

const style = bemCssModules(WarehouseStatsModuleCss);

export const WarehouseStats: React.FC<WarehouseStatsProps> = (props) => {
    const { name, totalCount, materials } = props.warehouse;

    const sortedMaterials = materials.sort((m1, m2) => m1.materialId.localeCompare(m2.materialId));

    return (
        <div className={style()}>
            <table>
                <thead>
                    <tr>
                        <th>{name}</th>
                        <th>(total {totalCount})</th>
                    </tr>
                </thead>
                <tbody>
                    {sortedMaterials.map((material) => (
                        <tr key={material.materialId}>
                            <td>{material.materialId}</td>
                            <td>{material.count}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};
