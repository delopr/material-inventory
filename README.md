## Material Inventory

# Development
To start development, type:  
`npm install`  
to install all packages and then:
`npm start`  
which will run development server.

# Build
Run command
`npm run build`
In output folder `dist` you will find production ready files

# Test
Command:  
`npm run test`